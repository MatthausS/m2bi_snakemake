# M2BI_snakemake



## Presentation
This project is a work done for my 3rd semester of my master degree.
It is a snakemake workflow which aim to analyse Gomez-Cabrero et al. 2019 (https://doi.org/10.1038/s41597-019-0202-7) rna-seq data.

## Prerequisites
In order to make this workflow works properly, you'll need to copy the tool environement:
```
	module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1 bowtie2/2.3.4.3
	conda env create -f workflow/envs/env.yml
```
You'll also need to have the same data folder as in the git:
bowtie index in data/bowtie2/all
the MM39 annoatation in data/flat/genomic.gtf
rna-seq files in data/rna-seq/ 

This pipeline works with this commande line write in the home of this git :
snakemake --cores 8 --jobs 100 --use-conda --latency-wait 20 --snakefile workflow/main.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=logs/slurm-%j.out"


## Results
All results are kept in the results folder with the name of his rule.
This analysis end with the count of features with htseq-count.

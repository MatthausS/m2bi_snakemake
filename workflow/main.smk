configfile: "config/config.yaml",
config=config["samples"]
rule all:
    input:
        expand("results/1.fastQC/{sample}_1_fastqc.html", sample = config),
        expand("results/1.fastQC/{sample}_2_fastqc.html", sample = config),
        expand("results/2.trimmo/{sample}_1_trimmed_paired.fastq.gz", sample = config),
        expand("results/2.trimmo/{sample}_1_trimmed_unpaired.fastq.gz", sample = config),
        expand("results/2.trimmo/{sample}_2_trimmed_paired.fastq.gz", sample = config),
        expand("results/2.trimmo/{sample}_2_trimmed_unpaired.fastq.gz", sample = config),
        expand("results/3.align/{sample}.mapped.sam", sample = config),
        expand("results/3.align/{sample}.sorted.bam", sample = config),
        expand("results/4.quantif/{sample}.htseq.gtf", sample = config)
rule fastqc:
    input:
        expand("data/rna-seq/{sample}_1.fastq.gz", sample = config),
        expand("data/rna-seq/{sample}_2.fastq.gz", sample = config)
    output:
        expand("results/1.fastQC/{sample}_1_fastqc.html", sample = config),
        expand("results/1.fastQC/{sample}_2_fastqc.html", sample = config),
        expand("results/1.fastQC/{sample}_1_fastqc.zip", sample = config),
        expand("results/1.fastQC/{sample}_2_fastqc.zip", sample = config)
    resources:
        mem_mb=5000,
        time="00:30:00"
    conda:
        "envs/env.yml"
    shell: "fastqc --outdir results/1.fastQC/ {input}"
rule trimmo:
    input:
        r1="data/rna-seq/{sample}_1.fastq.gz",
        r2="data/rna-seq/{sample}_2.fastq.gz"
    output:
        r1_paired="results/2.trimmo/{sample}_1_trimmed_paired.fastq.gz",
        r1_unpaired="results/2.trimmo/{sample}_1_trimmed_unpaired.fastq.gz",
        r2_paired="results/2.trimmo/{sample}_2_trimmed_paired.fastq.gz",
        r2_unpaired="results/2.trimmo/{sample}_2_trimmed_unpaired.fastq.gz" 
    resources:
        mem_mb=5000,
        time="00:30:00"
    conda:
        "envs/env.yml"
    shell: "java -jar /opt/apps/trimmomatic-0.38/trimmomatic-0.38.jar PE {input.r1} {input.r2} {output.r1_paired} {output.r1_unpaired} {output.r2_paired} {output.r2_unpaired}  ILLUMINACLIP:/opt/apps/trimmomatic-0.38/adapters/TruSeq3-PE.fa:2:30:10:2:True LEADING:3 TRAILING:3 MINLEN:36"
rule align:
    input:
        r1_paired="results/2.trimmo/{sample}_1_trimmed_paired.fastq.gz",
        r2_paired="results/2.trimmo/{sample}_2_trimmed_paired.fastq.gz"
    output:
        map="results/3.align/{sample}.mapped.sam"
    resources:
        mem_mb=5000,
        time="00:30:00"
    conda:
        "envs/env.yml"
    shell: "bowtie2 -x data/bowtie2/all -1 {input.r1_paired} -2 {input.r2_paired} -S {output.map}"
rule samtools:
    input:
        map="results/3.align/{sample}.mapped.sam"
    output:
        sort="results/3.align/{sample}.sorted.bam"
    resources:
        mem_mb=5000,
        time="00:30:00"
    conda:
        "envs/env.yml"
    shell: "samtools sort -n {input.map} -o {output.sort} "
rule htseq:
    input:
        sort="results/3.align/{sample}.sorted.bam"
    output:
        htseq="results/4.quantif/{sample}.htseq.gtf"
    resources:
        mem_mb=5000,
        time="00:30:00"
    conda:
        "envs/htseq.yml"
    shell: "htseq-count -f bam {input.sort} data/flat/genomic.gtf > {output.htseq}"
